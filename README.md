# OpenML dataset: padding-attack-dataset-2021-07-23-OpenSSL-1.1.1k-2023-04-28

https://www.openml.org/d/45750

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Bleichenbacher Padding Attack: Dataset created on 2023-04-28 with server 2021-07-23-OpenSSL-1.1.1k

Attribute Descriptions:

TLS0:tcp.srcport: TCP Source Port of the first TLS Alert
TLS0:tcp.dstport: TCP Destination Port of the first TLS Alert
TLS0:tcp.port: TCP Source or Destination Port of the first TLS Alert
TLS0:tcp.stream: TCP Stream index of the first TLS Alert
TLS0:tcp.len: TCP Segment Len of the first TLS Alert
TLS0:tcp.seq: TCP Sequence number of the first TLS Alert
TLS0:tcp.nxtseq: TCP Next sequence number of the first TLS Alert
TLS0:tcp.ack: TCP Acknowledgment number of the first TLS Alert
TLS0:tcp.hdr_len: TCP 1000 .... = Header Length of the first TLS Alert
TLS0:tcp.flags.res: TCP 000. .... .... = Reserved of the first TLS Alert
TLS0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first TLS Alert
TLS0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first TLS Alert
TLS0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first TLS Alert
TLS0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first TLS Alert
TLS0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first TLS Alert
TLS0:tcp.flags.push: TCP .... .... 1... = Push of the first TLS Alert
TLS0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first TLS Alert
TLS0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first TLS Alert
TLS0:tcp.flags.fin: TCP .... .... ...0 = Fin of the first TLS Alert
TLS0:tcp.window_size_value: TCP Window size value of the first TLS Alert
TLS0:tcp.window_size: TCP Calculated window size of the first TLS Alert
TLS0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first TLS Alert
TLS0:tcp.checksum.status: TCP Checksum Status of the first TLS Alert
TLS0:tcp.urgent_pointer: TCP Urgent pointer of the first TLS Alert
TLS0:tcp.options.nop: TCP tcp.options.nop of the first TLS Alert
TLS0:tcp.option_kind: TCP Kind of the first TLS Alert
TLS0:tcp.option_len: TCP Length of the first TLS Alert
TLS0:ssl.record.content_type: SSL Content Type of the first TLS Alert
TLS0:ssl.record.length: SSL Length of the first TLS Alert
TLS0:ssl.alert_message.level: SSL Level of the first TLS Alert
TLS0:ssl.alert_message.desc: SSL Description of the first TLS Alert
TLS0:order: Message order of the first TLS Alert within the server responses
DISC0:tcp.srcport: TCP Source Port of the first TCP Disconnect
DISC0:tcp.dstport: TCP Destination Port of the first TCP Disconnect
DISC0:tcp.port: TCP Source or Destination Port of the first TCP Disconnect
DISC0:tcp.stream: TCP Stream index of the first TCP Disconnect
DISC0:tcp.len: TCP Segment Len of the first TCP Disconnect
DISC0:tcp.seq: TCP Sequence number of the first TCP Disconnect
DISC0:tcp.nxtseq: TCP Next sequence number of the first TCP Disconnect
DISC0:tcp.ack: TCP Acknowledgment number of the first TCP Disconnect
DISC0:tcp.hdr_len: TCP 1000 .... = Header Length of the first TCP Disconnect
DISC0:tcp.flags.res: TCP 000. .... .... = Reserved of the first TCP Disconnect
DISC0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first TCP Disconnect
DISC0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first TCP Disconnect
DISC0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first TCP Disconnect
DISC0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first TCP Disconnect
DISC0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first TCP Disconnect
DISC0:tcp.flags.push: TCP .... .... 0... = Push of the first TCP Disconnect
DISC0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first TCP Disconnect
DISC0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first TCP Disconnect
DISC0:tcp.flags.fin: TCP .... .... ...1 = Fin of the first TCP Disconnect
DISC0:tcp.window_size_value: TCP Window size value of the first TCP Disconnect
DISC0:tcp.window_size: TCP Calculated window size of the first TCP Disconnect
DISC0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first TCP Disconnect
DISC0:tcp.checksum.status: TCP Checksum Status of the first TCP Disconnect
DISC0:tcp.urgent_pointer: TCP Urgent pointer of the first TCP Disconnect
DISC0:tcp.options.nop: TCP tcp.options.nop of the first TCP Disconnect
DISC0:tcp.option_kind: TCP Kind of the first TCP Disconnect
DISC0:tcp.option_len: TCP Length of the first TCP Disconnect
DISC0:order: Message order of the first TCP Disconnect within the server responses
DISC1:tcp.srcport: TCP Source Port of the second TCP Disconnect
DISC1:tcp.dstport: TCP Destination Port of the second TCP Disconnect
DISC1:tcp.port: TCP Source or Destination Port of the second TCP Disconnect
DISC1:tcp.stream: TCP Stream index of the second TCP Disconnect
DISC1:tcp.len: TCP Segment Len of the second TCP Disconnect
DISC1:tcp.seq: TCP Sequence number of the second TCP Disconnect
DISC1:tcp.nxtseq: TCP Next sequence number of the second TCP Disconnect
DISC1:tcp.ack: TCP Acknowledgment number of the second TCP Disconnect
DISC1:tcp.hdr_len: TCP 1000 .... = Header Length of the second TCP Disconnect
DISC1:tcp.flags.res: TCP 000. .... .... = Reserved of the second TCP Disconnect
DISC1:tcp.flags.ns: TCP ...0 .... .... = Nonce of the second TCP Disconnect
DISC1:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the second TCP Disconnect
DISC1:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the second TCP Disconnect
DISC1:tcp.flags.urg: TCP .... ..0. .... = Urgent of the second TCP Disconnect
DISC1:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the second TCP Disconnect
DISC1:tcp.flags.push: TCP .... .... 0... = Push of the second TCP Disconnect
DISC1:tcp.flags.reset: TCP .... .... .0.. = Reset of the second TCP Disconnect
DISC1:tcp.flags.syn: TCP .... .... ..0. = Syn of the second TCP Disconnect
DISC1:tcp.flags.fin: TCP .... .... ...0 = Fin of the second TCP Disconnect
DISC1:tcp.window_size_value: TCP Window size value of the second TCP Disconnect
DISC1:tcp.window_size: TCP Calculated window size of the second TCP Disconnect
DISC1:tcp.window_size_scalefactor: TCP Window size scaling factor of the second TCP Disconnect
DISC1:tcp.checksum.status: TCP Checksum Status of the second TCP Disconnect
DISC1:tcp.urgent_pointer: TCP Urgent pointer of the second TCP Disconnect
DISC1:tcp.options.nop: TCP tcp.options.nop of the second TCP Disconnect
DISC1:tcp.option_kind: TCP Kind of the second TCP Disconnect
DISC1:tcp.option_len: TCP Length of the second TCP Disconnect
DISC1:order: Message order of the second TCP Disconnect within the server responses
CKE0:tcp.srcport: TCP Source Port of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.dstport: TCP Destination Port of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.port: TCP Source or Destination Port of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.stream: TCP Stream index of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.len: TCP Segment Len of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.seq: TCP Sequence number of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.nxtseq: TCP Next sequence number of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.ack: TCP Acknowledgment number of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.hdr_len: TCP 1000 .... = Header Length of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.res: TCP 000. .... .... = Reserved of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.ns: TCP ...0 .... .... = Nonce of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.cwr: TCP .... 0... .... = Congestion Window Reduced (CWR) of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.ecn: TCP .... .0.. .... = ECN-Echo of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.urg: TCP .... ..0. .... = Urgent of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.ack: TCP .... ...1 .... = Acknowledgment of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.push: TCP .... .... 0... = Push of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.reset: TCP .... .... .0.. = Reset of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.syn: TCP .... .... ..0. = Syn of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.flags.fin: TCP .... .... ...0 = Fin of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.window_size_value: TCP Window size value of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.window_size: TCP Calculated window size of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.window_size_scalefactor: TCP Window size scaling factor of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.checksum.status: TCP Checksum Status of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.urgent_pointer: TCP Urgent pointer of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.options.nop: TCP tcp.options.nop of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.option_kind: TCP Kind of the first Client Key Exchange TCP Acknowledgement
CKE0:tcp.option_len: TCP Length of the first Client Key Exchange TCP Acknowledgement
CKE0:order: Message order of the first Client Key Exchange TCP Acknowledgement within the server responses

vulnerable_classes []

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45750) of an [OpenML dataset](https://www.openml.org/d/45750). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45750/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45750/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45750/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

